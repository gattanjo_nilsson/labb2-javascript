$.getJSON('https://iths.bbweb.se/articles?key=8f2108c8-7a01-4b53-9ff0-f3c14b68b47a', function(data) {
    console.log(data);
    for (var i = 0; i < data.length; i++) {
        var id = data[i]._id;
        var title = data[i].title;
        //console.log(id);
        var btn = document.createElement("BUTTON");
        btn.setAttribute("id", id);
        //console.log(btn.id);
        var t = document.createTextNode(title);
        btn.appendChild(t);
        $('#menu').append(btn);
    }

    $('button').on('click', function() {

        if (this.id === "noArticle") {
            $('h1').text("No article selected");
            $('#content').hide();
        } else {
            var identity = this.id;
            console.log(identity);
            var key = '?key=8f2108c8-7a01-4b53-9ff0-f3c14b68b47a';
            var query = 'https://iths.bbweb.se/articles/' + identity + key;

            $('#laddar').text("Laddar...");
            $.getJSON(query, function(article) {
                console.log(article);
                $('h1').text(article.title);
                $('#author').text(article.author);
                $('#date').text(article.date);
                $('#text').html(article.text);
                $('#content').show();
                $('#laddar').text("");

            });
        }
    });
});